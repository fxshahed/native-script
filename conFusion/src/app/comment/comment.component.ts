import { Component, OnInit } from '@angular/core';
import { Comment } from '../shared/comment';
import { Slider } from "tns-core-modules/ui/slider";
import { TextField } from "tns-core-modules/ui/text-field";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalDialogService, ModalDialogOptions, ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    moduleId: module.id,
    templateUrl: 'comment.component.html'
})
export class CommentComponent implements OnInit {
    comment: Comment;
    commentForm: FormGroup;
    ratingValue: number = 5;

    constructor(
        private formBuilder: FormBuilder, 
        private params: ModalDialogParams
        ) {
        this.commentForm = this.formBuilder.group({
            rating: 5,
            author: ['', Validators.required],
            comment: ['', Validators.required]
        });
    }

    ngOnInit() { }

    onSubmit(){
        this.comment = <Comment>this.commentForm.value;
        this.comment.date = new Date().toISOString();
        this.params.closeCallback(this.comment);
    }
}