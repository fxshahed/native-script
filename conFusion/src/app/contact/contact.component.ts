import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: 'contact.component.html'
})
export class ContactComponent implements OnInit {
    contactInfo: string = "121, Clear Water Bay Road\nClear Water Bay, Kowloon\nHONG KONG\nTel: +852 1234 5678\nFax: +852 8765 4321\nEmail:confusion@food.net";

    constructor() { }

    ngOnInit() { }
}