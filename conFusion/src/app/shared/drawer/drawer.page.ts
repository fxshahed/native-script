import { ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { RadSideDrawerComponent, SideDrawerType } from 'nativescript-ui-sidedrawer/angular';


export class DrawerPage implements AfterViewInit {
    ngAfterViewInit(): void {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDectorRef.detectChanges();
    }


    protected openDrawer() {
        this.drawer.showDrawer();
    }

    protected closeDrawer() {
        this.drawer.closeDrawer();
    }

    @ViewChild(RadSideDrawerComponent) protected drawerComponent: RadSideDrawerComponent;

    protected drawer: SideDrawerType;
    constructor(private _changeDectorRef: ChangeDetectorRef) {



    }
}