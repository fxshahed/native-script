import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { Dish } from '../app/shared/dish';
import { DishService } from '~/app/services/dish.service';

import { DrawerPage } from '../app/shared/drawer/drawer.page';




@Component({
  selector: 'app-menu',
  moduleId: module.id,
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent extends DrawerPage implements OnInit {

  dishes: Dish[];
  errMess: string;

  constructor(private dishService: DishService,
    private ChangeDetectorRef: ChangeDetectorRef,
    @Inject('baseURL') private baseURL) { 
      super(ChangeDetectorRef)
    }

  ngOnInit() {
    this.dishService.getDishes()
      .subscribe(dishes => this.dishes = dishes,
        errmess => this.errMess = <any>errmess);
  }

}